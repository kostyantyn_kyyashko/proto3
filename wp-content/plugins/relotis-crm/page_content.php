<?php
/*
* Created by Kostyantyn Kyyashko
* k.konstantin.n@gmail.com
*/

function month_to_time()
{
    $year = date('Y', time());
    $next_year = $year + 1;
    $begin_of_current_year = strtotime("1/1/$year");
    $month_array[1] = $begin_of_current_year;
    for ($month =1; $month <= 13; $month++) {
        if ($month < 13) {
            $month_array[$month] = strtotime("$month/1/$year");
        }
        else{
            $month_array[$month] = strtotime("1/1/$next_year");
        }
    }
    return $month_array;
}
global $month_to_time;
$month_to_time = month_to_time();

function data_from_db($from_time, $finish_time)
{
    global $month_to_time;
    global $wpdb;
    $out=[];
    $sql = <<<sql
SELECT COUNT(ca.transaction_id) count, SUM(ca.price_value) sum FROM crm_analysis ca
WHERE ca.close_at > {$from_time} AND ca.close_at < {$finish_time}
sql;
       $result = $wpdb->get_row($sql);
       $count = $result->count;
       $sum = $result->sum;
       $out['count'] = $count;
       $out['sum'] = $sum;
    return $out;
}

function transactions_data()
{
    global $month_to_time;
    for($i = 1; $i < 13; $i++) {
        $from_time = $month_to_time[$i];
        $finish_time = $month_to_time[$i+1];
        $superout = data_from_db($from_time, $finish_time);
        $count[] = $superout['count'];
        $sum[] = $superout['sum'];
    }
    $chart_counter = implode(',', $count);
    $sum_counter = implode(',', $sum);
    ?><h3>Total Statistic & Data for current Year</h3><?
    do_shortcode('[common_chart_css_js]');
    do_shortcode('[area_chart chart_values="' . $chart_counter .'" title="Deals by Month" label="Total sum of Closed Deal"]');
    do_shortcode('[area_chart3 chart_values="' . $sum_counter .'" title="Sum by Month" label="Total sum of Closed Deal" ]');

    ?>
    <table class="table">
        <tbody>
        <tr>
            <td style="width: 50%">
                <?=do_shortcode('[sema4 title="Current Stage Statuses"]')?>
            </td>
            <td id="meter" style="width: 50%; padding-bottom: 100px;">
                <link rel="stylesheet" href="http://proto3.relotis.com/wp-content/plugins/relotis-charts/css/all.min.css">
                <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
                <link rel="stylesheet" href="http://proto3.relotis.com/wp-content/plugins/relotis-charts/css/adminlte.min.css">
                <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
                <script src="http://proto3.relotis.com/wp-content/plugins/relotis-charts/js/jquery.min.js"></script>
                <script src="http://proto3.relotis.com/wp-content/plugins/relotis-charts/js/bootstrap.bundle.min.js"></script>
                <script src="http://proto3.relotis.com/wp-content/plugins/relotis-charts/js/Charts.min.js"></script>
                <script src="http://proto3.relotis.com/wp-content/plugins/relotis-charts/js/adminlte.min.js"></script>
                <script src="http://proto3.relotis.com/wp-content/plugins/relotis-charts/js/demo.js"></script>
                <link rel="stylesheet" href="http://proto3.relotis.com/wp-content/plugins/virtual_meter/css/jqx.base.css" type="text/css">
                <script type="text/javascript" src="http://proto3.relotis.com/wp-content/plugins/virtual_meter/js/jquery-3.5.1.min.js"></script>
                <script type="text/javascript" src="http://proto3.relotis.com/wp-content/plugins/virtual_meter/js/jqxcore.js"></script>
                <script type="text/javascript" src="http://proto3.relotis.com/wp-content/plugins/virtual_meter/js/jqxdraw.js"></script>
                <script type="text/javascript" src="http://proto3.relotis.com/wp-content/plugins/virtual_meter/js/jqxgauge.js"></script>
                <link rel="stylesheet" href="http://proto3.relotis.com/wp-content/plugins/virtual_meter/css/gaugeValue.css" type="text/css">
                <script>
                    var delta = 17;
                    var ranges= [{ startValue: 0, endValue: delta, style: { fill: '#e02629', stroke: '#e02629' }, endWidth: 10, startWidth: 10 },
                        { startValue: delta , endValue: delta*2, style: { fill: '#ff8000', stroke: '#ff8000' }, endWidth: 10, startWidth: 10 },
                        { startValue: delta*2, endValue: delta*3, style: { fill: '#fbd109', stroke: '#fbd109' }, endWidth: 10, startWidth: 10 },
                        { startValue: delta*3, endValue: delta*4, style: { fill: '#a6e091', stroke: '#83E077' }, endWidth: 10, startWidth: 10 },
                        { startValue: delta*4, endValue: delta*5, style: { fill: '#4bb648', stroke: '#4bb648' }, endWidth: 10, startWidth: 10 },
                        { startValue: delta*5, endValue:100, style: { fill: '#4bb648', stroke: '#4bb648' }, endWidth:10, startWidth: 10 }
                    ];
                    $(document).ready(function () {
                        $('#gaugeContainer').jqxGauge({
                            ranges: ranges,
                            ticksMinor: { interval: 5, size: '5%' },
                            ticksMajor: { interval: 10, size: '9%' },
                            value: 0,
                            colorScheme: 'scheme05',
                            endAngle: 270,
                            animationDuration: 1200,
                            max: 100,
                            width: 200,
                            height: 200,
                        });

                        $('#gaugeContainer').on('valueChanging', function (e) {
                            $('#gaugeValue').remove();
                        });

                        $('#gaugeContainer').jqxGauge('value', 25);

                    });
                </script>
                <div id="demoWidget" style="position: relative;">
                    <div style="float: left; width: 200px; height: 200px;" id="gaugeContainer" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" aria-disabled="false" class="jqx-widget"><div style="width: 200px; height: 200px;"><table class="tblChart" cellspacing="0" cellpadding="0" border="0" align="left" valign="top"><tbody><tr><td colspan="2" class="tdTop" style="height: 0.5px;"><div style="padding-left: 36px;">Performance Indicator</div></td></tr><tr><td class="tdLeft" style="width: 0.5px;"></td><td><div class="chartContainer" style="position: relative; width: 199px; height: 199px; overflow: hidden;" onselectstart="return false;"><svg id="svgChart" version="1.1" width="100%" height="100%" overflow="hidden"><defs><linearGradient x1="0%" y1="0%" x2="0%" y2="100%" id="grd1594497753217BABABAv"><stop offset="0%" style="stop-color:#BABABA"></stop><stop offset="25%" style="stop-color:#CDCDCD"></stop><stop offset="50%" style="stop-color:#FFFFFF"></stop><stop offset="100%" style="stop-color:#BABABA"></stop></linearGradient><linearGradient x1="0%" y1="0%" x2="0%" y2="100%" id="grd1594497753217ffffffv"></linearGradient></defs><circle cx="97.5" cy="97.5" r="97" stroke="#cccccc" fill="url(http://proto3.relotis.com/wp-admin/admin.php?page=relotis_user2#grd1594497753217BABABAv)"></circle><circle cx="97.5" cy="97.5" r="88" fill="#ffffff" stroke="#E0E0E0"></circle><path d="M24.753866082107123,139.49999999999994 A84,84 100 0,1 14.987870938790167,81.75996957479906 L 24.810743446077055,83.63378272065631 A74,74 100 0,0 33.414120119951505,134.49999999999994 L 24.753866082107123,139.49999999999994 z" fill="#e02629" stroke="#e02629" visibility="visible" class="jqx-gauge-range"></path><path d="M14.987870938790167,81.75996957479906 A84,84 100 0,1 45.32358645662193,31.669749584629457 L 51.53506425940504,39.506684157887854 A74,74 100 0,0 24.810743446077055,83.63378272065631 L 14.987870938790167,81.75996957479906 z" fill="#ff8000" stroke="#ff8000" visibility="visible" class="jqx-gauge-range"></path><path d="M45.32358645662193,31.669749584629457 A84,84 100 0,1 101.01755491325282,13.573682271695901 L 100.59879837596083,23.564910572684482 A74,74 100 0,0 51.53506425940504,39.506684157887854 L 45.32358645662193,31.669749584629457 z" fill="#fbd109" stroke="#fbd109" visibility="visible" class="jqx-gauge-range"></path><path d="M101.01755491325282,13.573682271695901 A84,84 100 0,1 155.00195689800987,36.26663529660145 L 148.15648583872297,43.55632157081556 A74,74 100 0,0 100.59879837596083,23.564910572684482 L 101.01755491325282,13.573682271695901 z" fill="#a6e091" stroke="#83E077" visibility="visible" class="jqx-gauge-range"></path><path d="M155.00195689800987,36.26663529660145 A84,84 100 0,1 181.03983921093499,88.71960908551713 L 171.09462025725225,89.76489371819368 A74,74 100 0,0 148.15648583872297,43.55632157081556 L 155.00195689800987,36.26663529660145 z" fill="#4bb648" stroke="#4bb648" visibility="visible" class="jqx-gauge-range"></path><path d="M181.03983921093499,88.71960908551713 A84,84 100 0,1 170.24613391789282,139.50000000000006 L 161.58587988004842,134.50000000000006 A74,74 100 0,0 171.09462025725225,89.76489371819368 L 181.03983921093499,88.71960908551713 z" fill="#4bb648" stroke="#4bb648" visibility="visible" class="jqx-gauge-range"></path><line x1="36.01219633130483" y1="132.99999999999994" x2="42.0743741577959" y2="129.49999999999994" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="26.888945428852594" y1="104.92152089200336" x2="33.8505986964305" y2="104.1898216491298" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="29.97498734304409" y1="119.44020660062125" x2="33.779213408224706" y2="118.20413862312147" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-minor"></line><line x1="29.974987343044106" y1="75.55979339937872" x2="36.632382957110174" y2="77.72291236000336" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="44.736717391105046" y1="49.99172694852103" x2="49.938731169446804" y2="54.67564119303304" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="26.888945428852594" y1="90.07847910799654" x2="30.867033010325684" y2="90.49659296106717" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-minor"></line><line x1="68.62169834161821" y1="32.63827250737532" x2="71.46885484314882" y2="39.03309071087353" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="97.50000000000004" y1="26.5" x2="97.50000000000004" y2="33.5" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="36.01219633130487" y1="61.99999999999997" x2="39.47629794644262" y2="63.99999999999997" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-minor"></line><line x1="126.37830165838186" y1="32.63827250737535" x2="123.53114515685125" y2="39.03309071087356" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="150.26328260889503" y1="49.99172694852111" x2="145.06126883055327" y2="54.675641193033115" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="55.76724708723444" y1="40.059793399378705" x2="58.11838809640433" y2="43.2958613768785" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-minor"></line><line x1="165.02501265695594" y1="75.5597933993788" x2="158.36761704288983" y2="77.72291236000342" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="168.1110545711474" y1="104.92152089200344" x2="161.1494013035695" y2="104.18982164912987" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="82.73826995193915" y1="28.051520347899782" x2="83.56991671521017" y2="31.964110750835005" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-minor"></line><line x1="158.9878036686951" y1="133.00000000000006" x2="152.92562584220406" y2="129.50000000000006" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-major"></line><line x1="112.26173004806094" y1="28.05152034789981" x2="111.4300832847899" y2="31.964110750835033" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-minor"></line><line x1="139.23275291276565" y1="40.059793399378776" x2="136.88161190359574" y2="43.29586137687856" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-minor"></line><line x1="158.98780366869516" y1="62.000000000000064" x2="155.52370205355743" y2="64.00000000000006" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-minor"></line><line x1="168.1110545711474" y1="90.07847910799663" x2="164.13296698967432" y2="90.49659296106724" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-minor"></line><line x1="165.02501265695588" y1="119.44020660062131" x2="161.22078659177527" y2="118.20413862312151" stroke="#898989" stroke-width="1" class="jqx-gauge-tick-minor"></line><text class="jqx-gauge-label" cursor="default" x="45" y="132" width="10" height="12">0</text><text class="jqx-gauge-label" cursor="default" x="35" y="88" width="20" height="12">20</text><text class="jqx-gauge-label" cursor="default" x="65" y="54" width="20" height="12">40</text><text class="jqx-gauge-label" cursor="default" x="110" y="54" width="20" height="12">60</text><text class="jqx-gauge-label" cursor="default" x="140" y="88" width="20" height="12">80</text><text class="jqx-gauge-label" cursor="default" x="131" y="132" width="29" height="12">100</text><text class="jqx-gauge-caption" cursor="default" x="98" y="146" width="0" height="0"></text><path d="M 96.5,97.86602540378443 L 97.5,96.13397459621557 L 44.17245036914925,66.49999999999997" z-index="0" stroke="#25A0DA" fill="#25A0DA" stroke-width="1" style="visibility: visible;"></path><circle cx="97.5" cy="97.5" r="3" fill="#25A0DA" stroke-width="1px" stroke="#25A0DA" z-index="30" style="visibility: visible;"></circle></svg></div></td></tr></tbody></table></div></div>

                    <div style="margin-left: 60px; float: left;" id="linearGauge"></div>
                </div>


                <div style="position: absolute; bottom: 5px; right: 5px; display: none;">
                    <a href="https://www.jqwidgets.com/" alt="https://www.jqwidgets.com/"><img alt="https://www.jqwidgets.com/" title="https://www.jqwidgets.com/" src="https://www.jqwidgets.com/wp-content/design/i/logo-jqwidgets.png"></a>
                </div>
            </td>
        </tr>
        </tbody>
    </table>
    <?do_shortcode('[area_chart2 chart_values="' . $sum_counter .'"]');?>
    <?
    global $wpdb;
    $sql = "SELECT * FROM crm_analysis ORDER BY begin_at DESC";
    $rows = $wpdb->get_results($sql);
    ?>
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#deals"><h3>Deals</h3></a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#agents"><h3>Agents</h3></a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane show active" id="deals">
            <table class="table table-bordered table-striped table-hover" style="width: 800px;">
                <thead>
                <tr>
                    <th style="width: 70px">URL in<br>Listing</th>
                    <th>URL<br>CRM</th>
                    <th>Owner</th>
                    <th>Price</th>
                    <th>Address</th>
                    <th>Agent</th>
                    <th>Begin at</th>
                    <th>Stage</th>
                </tr>
                </thead>
                <tbody>
                <?foreach ($rows as $row):?>
                    <tr>
                        <td><a target="_blank" href="<?=$row->object_url?>">URL</a> </td>
                        <td><a href="http://proto3.relotis.com/wp-content/plugins/relotis-crm/linker.php?transaction_id=<?=$row->transaction_id?>&wp_user_id=<?=$row->wp_user_id?>&wp_user_name=<?=$row->wp_user_name?>&status=<?=$row->status?>">

                                <?=$row->transaction_name?></a> </td>
                        <td><?=$row->crm_owner?></td>
                        <td><?=$row->price_value?></td>
                        <td><?=$row->address?></td>
                        <td><?=$row->wp_user_name?></td>
                        <td><?=date('d/m/Y', $row->begin_at)?></td>
                        <td><?=$row->status?></td>
                    </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="tab-pane show" id="agents">
            <?
            $wp_user_names = $wpdb->get_col("SELECT crm.wp_user_name agent FROM crm_analysis crm GROUP BY crm.wp_user_name");
            function _sum($wp_user_name)
            {
                global $wpdb;
                return $wpdb->get_var("SELECT SUM(crm.price_value) sum FROM crm_analysis crm WHERE crm.wp_user_name ='{$wp_user_name}' GROUP BY crm.wp_user_name");
            }
            function _begin($wp_user_name)
            {
                global $wpdb;
                return $wpdb->get_var("SELECT COUNT(crm.status) status FROM crm_analysis crm WHERE crm.status = 'Begin' AND crm.wp_user_name ='{$wp_user_name}' GROUP BY crm.wp_user_name");
            }
            function _analysis($wp_user_name)
            {
                global $wpdb;
                return $wpdb->get_var("SELECT COUNT(crm.status) status FROM crm_analysis crm WHERE crm.status = 'Analysis' AND crm.wp_user_name ='{$wp_user_name}' GROUP BY crm.wp_user_name");
            }
            function _proposal($wp_user_name)
            {
                global $wpdb;
                return $wpdb->get_var("SELECT COUNT(crm.status) status FROM crm_analysis crm WHERE crm.status = 'Proposal' AND crm.wp_user_name ='{$wp_user_name}' GROUP BY crm.wp_user_name");
            }
            function _negotiations($wp_user_name)
            {
                global $wpdb;
                return $wpdb->get_var("SELECT COUNT(crm.status) status FROM crm_analysis crm WHERE crm.status = 'Negotiations' AND crm.wp_user_name ='{$wp_user_name}' GROUP BY crm.wp_user_name");
            }
            function _close($wp_user_name)
            {
                global $wpdb;
                return $wpdb->get_var("SELECT COUNT(crm.status) status FROM crm_analysis crm WHERE crm.status = 'Close' AND crm.wp_user_name ='{$wp_user_name}' GROUP BY crm.wp_user_name");
            }
            ?>
            <table class="table table-bordered table-striped table-hover">
                <thead>
                <th>
                    <tr>
                        <th>Agent</th>
                        <th>SUM</th>
                        <th>Begin</th>
                        <th>Analysis</th>
                        <th>Proposal</th>
                        <th>Negotiations</th>
                        <th>Close</th>
                    </tr>
                </th>
                </thead>
                <tbody>
                <?foreach ($wp_user_names as $wp_user_name):?>
                <tr>
                    <td><?=$wp_user_name?></td>
                    <td><?=_sum($wp_user_name)?></td>
                    <td><?=_begin($wp_user_name)?></td>
                    <td><?=_analysis($wp_user_name)?></td>
                    <td><?=_proposal($wp_user_name)?></td>
                    <td><?=_negotiations($wp_user_name)?></td>
                    <td><?=_close($wp_user_name)?></td>
                </tr>
                <?endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
    <?
}
